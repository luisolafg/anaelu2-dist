echo
echo This Source Code Form is subject to the terms of the Mozilla Public
echo License, v. 2.0. If a copy of the MPL was not distributed with this
echo file, You can obtain one at http://mozilla.org/MPL/2.0/.
echo

set ALL_MAIN=%cd%

cd  cli

call  build_crysfml_n_anaelu_cli.bat

cd %ALL_MAIN%\gui

echo my_dir:

call build_n_run_cut_imgs.bat
call build_n_run_scale_img.bat
call build_n_run_smooth_imgs.bat
call build_n_run_sum_imgs.bat
call build_n_run_diff_imgs.bat
call build_n_run_ex_mark.bat
call build_n_run_rot_imgs.bat

cd %ALL_MAIN%

rd /s /q tmp_exe
rd /s /q tmp_exe

md tmp_exe

copy gui\*.exe tmp_exe
copy cli\anaelu_cli_tools\*.exe tmp_exe
copy cli\format_tools\*.exe tmp_exe
cd tmp_exe

set EXE_PATH=%cd%
cd %ALL_MAIN%
python build_path.py %EXE_PATH%

echo Done building

