#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

export ALL_MAIN=$(pwd)

cd  cli
echo "building crysfml and anaelu_cli"
./build_crysfml_n_anaelu_cli.sh

cd $ALL_MAIN/gui
export GUI_PATH=$(pwd)
echo "building image treatment CLI tools"
./build_n_run_cut_imgs.sh
./build_n_run_scale_img.sh
./build_n_run_smooth_imgs.sh
./build_n_run_sum_imgs.sh
./build_n_run_diff_imgs.sh
./build_n_run_ex_mark.sh
./build_n_run_rot_imgs.sh
cd $ALL_MAIN

echo "creating a clean tmp_exe dir"
rm -rf tmp_exe
mkdir tmp_exe

echo "copying CLI tools to tmp exe dir"
cp gui/*.exe tmp_exe
cp cli/anaelu_cli_tools/*.exe tmp_exe
cp cli/format_tools/*.exe tmp_exe
cd tmp_exe

echo "final PATH tweaks"
export EXE_PATH=$(pwd)
cd $ALL_MAIN
echo "path to re-use = " $EXE_PATH
python3 build_path_lin.py $EXE_PATH $GUI_PATH

chmod +x anaelu_gui
mv anaelu_gui $EXE_PATH
echo Done building


