
program img_smooth

    use input_arg_tools,                only: handling_kargs, arg_def_list
    use edf_utils,                      only: get_efd_info, write_edf
    use inner_tools,                    only: resid, img_out, r, r_2

    implicit none

    character(len=9999)                             :: str_data
    real(kind = 8), dimension(:,:), allocatable     :: calc_img, exp_img, res_img
    Type(arg_def_list)              :: my_def



    my_def%num_of_vars = 4
    allocate(my_def%field(1:my_def%num_of_vars))
    my_def%field(1)%var_name = "img_calc_in"
    my_def%field(1)%value = "my_calc_in.edf"

    my_def%field(2)%var_name = "img_exp_in"
    my_def%field(2)%value = "my_exp_in.edf"

    my_def%field(3)%var_name = "dif_img_out"
    my_def%field(3)%value = "diff_img.edf"

    my_def%field(4)%var_name = "res_data_out"
    my_def%field(4)%value = "img_diff_info.dat"

    call handling_kargs(my_def)

    write(*,*) "img_calc_in: ", my_def%field(1)%value
    write(*,*) "img_exp_in: ", my_def%field(2)%value
    write(*,*) "dif_img_out: ", my_def%field(3)%value
    write(*,*) "res_data_out: ", my_def%field(4)%value

    call get_efd_info(my_def%field(1)%value, str_data, calc_img)
    write(*,*) "header:", trim(str_data)

    call get_efd_info(my_def%field(2)%value, str_data, exp_img)
    write(*,*) "header:", trim(str_data)

    call resid(calc_img, exp_img)

    call write_edf(my_def%field(3)%value, img_out)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    OPEN(unit = 10, file = my_def%field(4)%value, status = "replace", form = "formatted")
        write(unit = 10,fmt=*) "r =", r
        write(unit = 10,fmt=*) "r_2 =", r_2
    close(unit = 10)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


end program img_smooth
