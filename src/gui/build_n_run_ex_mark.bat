echo removing previous binrs, objs and imgs
del /f anaelu_img_ex_mark marked_img.edf
del /f anaelu_img_ex_mark.exe
del /f *.o *.mod

echo compiling
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall input_args.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall rw_n_use_edf.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall tools.f90

gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall anaelu_img_ex_mark.f90

echo linking
gfortran -o anaelu_img_ex_mark.exe *.o -static

del /f *.o *.mod


