echo "removing previous binrs, objs and imgs"
rm anaelu_img_ex_mark marked_img.edf
rm anaelu_img_ex_mark.exe
rm *.o *.mod

echo "compiling"
gfortran -c -Wall input_args.f90
gfortran -c -Wall rw_n_use_edf.f90
gfortran -c -Wall tools.f90

gfortran -c -Wall anaelu_img_ex_mark.f90

echo "linking"
gfortran -o anaelu_img_ex_mark.exe *.o -static

echo "running binary exe"
#./anaelu_img_ex_mark.exe ../../Example/APT73_ddd_no_direct_beam.edf marked_img.edf 1507,1537,1022,1065 circumference
#./anaelu_img_ex_mark.exe ../../Example/APT73_ddd_no_direct_beam.edf marked_img.edf 1515,1474,880,835 circumference
#./anaelu_img_ex_mark.exe ../../Example/APT73_ddd_no_direct_beam.edf marked_img.edf 788,842,868,824 circumference
#./anaelu_img_ex_mark.exe ../../Example/APT73_ddd_no_direct_beam.edf marked_img.edf 786,763,1006,1070 circumference
./anaelu_img_ex_mark.exe ../../Example/img_xrd.edf marked_img.edf 1885,2159,247,496 rectangle
echo "done"
rm *.o *.mod
