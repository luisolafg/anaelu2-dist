echo removing previous binrs, objs and imgs
del /f rot_img marked_img.edf
del /f anaelu_rot_tool.exe
del /f *.o *.mod

echo compiling
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall input_args.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall rw_n_use_edf.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall tools.f90

gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall rot_img.f90

echo linking
gfortran -o anaelu_rot_tool.exe *.o -static

del /f *.o *.mod
