import numpy as np
import fabio
from matplotlib import pyplot as plt

img_arr = fabio.open("cuted.edf").data.astype(np.float64)
plt.imshow(img_arr, interpolation = "nearest" )
plt.show()

#res_img = fabio.edfimage.edfimage()
#res_img.data = img_arr
#res_img.write("dummy.edf")
