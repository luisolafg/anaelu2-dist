#!/usr/bin/python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

class InstrumentData(object):

    '''functionless data class'''
    def __init__(self):

        self.x_beam   = None
        self.y_beam   = None
        self.x_res    = None
        self.y_res    = None
        self.lambd    = None
        self.dst_det  = None
        self.diam_det = None
        self.up_hfl   = None

def read_dat_file(my_file_path = "param.dat"):

    '''opening the .cfl file to read the experiment parameters data'''
    myfile = open(my_file_path, "r")
    all_lines = myfile.readlines()
    myfile.close()

    lst_dat = []

    for lin_char in all_lines:

        commented = False

        for delim in ',;=:/':    # filtering unwanted data in the .cfl file as ,:=;
            lin_char = lin_char.replace(delim, ' ')

        for litle_block in lin_char.split():    # filtering the commented lines
            if( litle_block == "!" or litle_block == "#" ):
                commented = True
            if( not commented and litle_block != ","):
                lst_dat.append(litle_block)

    dt = InstrumentData()
    dt.up_hfl = False

    for pos, dat_memb in enumerate(lst_dat):
        if(dat_memb == "xres"):
            dt.x_res    = lst_dat[pos + 1]

        elif(dat_memb == "yres"):
            dt.y_res    = lst_dat[pos + 1]

        elif(dat_memb == "xbeam"):
            dt.x_beam    = lst_dat[pos + 1]

        elif(dat_memb == "ybeam"):
            dt.y_beam    = lst_dat[pos + 1]

        elif(dat_memb == "lambda"):
            dt.lambd = lst_dat[pos + 1]

        elif(dat_memb == "dst_det"):
            dt.dst_det = lst_dat[pos + 1]

        elif(dat_memb == "diam_det"):
            dt.diam_det = lst_dat[pos + 1]

        elif(dat_memb == "thet_min"):
            dt.thet_min = lst_dat[pos + 1]

        elif(dat_memb == "UPPER_HALF"):
            dt.up_hfl = True

    return dt


def write_dat(dto):

    '''writing the experiment parameters in the param.dat file, read from the gui'''

    myfile = open("param.dat", "w")
    wrstring = "xres" + "\n" + dto.x_res + "\n"
    myfile.write(wrstring);
    wrstring = "yres" + "\n" + dto.y_res + "\n"
    myfile.write(wrstring);
    wrstring =  "xbeam" + "\n" + dto.x_beam + "\n"
    myfile.write(wrstring);
    wrstring = "ybeam" + "\n" + dto.y_beam + "\n"
    myfile.write(wrstring);
    wrstring = "lambda" + "\n" + dto.lambd + "\n"
    myfile.write(wrstring);
    wrstring = "dst_det" + "\n" + dto.dst_det + "\n"
    myfile.write(wrstring);
    wrstring = "diam_det" + "\n" + dto.diam_det + "\n"
    myfile.write(wrstring);
    wrstring = "thet_min" + "\n" + "0" + "\n"
    myfile.write(wrstring);

    if(dto.up_hfl == True):
        myfile.write("UPPER_HALF" + "\n");

    myfile.write("end" + "\n\n");

    myfile.close()

if( __name__ == "__main__" ):
    dat_file = read_dat_file(my_file_path = "param.dat")
