The << tmp_files >> directory is a placeholder for Anaelu 2.0 temporal working files.

This file is here just to make git able to add a directory

You can copy from here but modifying something is strongly discouraged.
